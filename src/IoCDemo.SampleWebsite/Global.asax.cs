﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using IoCDemo.SampleWebsite.IoC;

namespace IoCDemo.SampleWebsite
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var container = new IoCDemoContainer();
            BootStrapper.Configure(container);
            
            ControllerBuilder.Current.SetControllerFactory(new IoCDemoControllerFactory(container));
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
        }
    }
}
