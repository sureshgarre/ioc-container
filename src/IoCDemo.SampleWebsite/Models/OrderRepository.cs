using IoCDemo.SampleWebsite.Interfaces;

namespace IoCDemo.SampleWebsite.Models
{
    public class OrderRepository : IOrderRepository
    {
        private static int nextOrderId;

        public int Insert(Order order)
        {
            return ++nextOrderId;
        }
    }
}