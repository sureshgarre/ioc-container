﻿using IoCDemo.SampleWebsite.Interfaces;

namespace IoCDemo.SampleWebsite.Models
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        public int Create(Order order)
        {
            int orderId = orderRepository.Insert(order);
            return orderId;
        }
    }
}
