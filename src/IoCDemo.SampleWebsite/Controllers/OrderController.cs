using System.Web.Mvc;
using IoCDemo.SampleWebsite.Interfaces;
using IoCDemo.SampleWebsite.Models;

namespace IoCDemo.SampleWebsite.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService orderService;

        public OrderController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(int productId)
        {
            int orderId = orderService.Create(new Order(productId));

            ViewData["OrderId"] = orderId;

            return View();
        }
    }
}