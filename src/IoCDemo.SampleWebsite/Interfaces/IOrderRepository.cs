using IoCDemo.SampleWebsite.Models;

namespace IoCDemo.SampleWebsite.Interfaces
{
    public interface IOrderRepository
    {
        int Insert(Order order);
    }
}