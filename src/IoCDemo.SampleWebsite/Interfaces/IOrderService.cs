using IoCDemo.SampleWebsite.Models;

namespace IoCDemo.SampleWebsite.Interfaces
{
    public interface IOrderService
    {
        int Create(Order order);
    }
}