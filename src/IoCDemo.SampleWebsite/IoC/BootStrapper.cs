using IoCDemo.interfaces;
using IoCDemo.SampleWebsite.Controllers;
using IoCDemo.SampleWebsite.Interfaces;
using IoCDemo.SampleWebsite.Models;

namespace IoCDemo.SampleWebsite.IoC
{
    public static class BootStrapper
    {
        public static void Configure(IContainer container)
        {
            container.Register<OrderController, OrderController>(LifeCycle.Transient);
            container.Register<IOrderService, OrderService>(LifeCycle.Transient);
            container.Register<IOrderRepository, OrderRepository>(LifeCycle.Transient);
        }
    }
}