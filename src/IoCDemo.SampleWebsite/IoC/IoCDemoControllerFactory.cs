﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using IoCDemo.interfaces;

namespace IoCDemo.SampleWebsite.IoC
{
    public class IoCDemoControllerFactory : DefaultControllerFactory
    {
        private readonly IContainer container;

        public IoCDemoControllerFactory(IContainer container)
        {
            this.container = container;
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return container.Resolve(controllerType) as Controller;
        }
    }
}