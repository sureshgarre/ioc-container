using System;
using NUnit.Framework;

namespace IoCDemo.Tests
{
    [TestFixture]
    public class IoCDemoContainerTests
    {
        [Test]
        public void Should_Resolve_Object()
        {
            //Arrange
            var container = new IoCDemoContainer();
            container.Register<ITypeToResolve, ConcreteType>(LifeCycle.Transient);

            //Act
            var instance = container.Resolve<ITypeToResolve>();

            //Assert
            Assert.That(instance, Is.InstanceOf(typeof(ConcreteType)));
        }

        [Test]
        public void Should_Throw_Exception_If_Type_Is_Not_Registered()
        {
            //Arrange
            var container = new IoCDemoContainer();
            Exception exception = null;

            //Act
            try
            {
                container.Resolve<ITypeToResolve>();
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            //Assert
            Assert.That(exception, Is.InstanceOf(typeof(TypeNotRegisteredException)));
        }

        [Test]
        public void Should_Resolve_Object_With_Registered_Constructor_Parameters()
        {
            //Arrange
            var container = new IoCDemoContainer();
            container.Register<ITypeToResolve, ConcreteType>(LifeCycle.Transient);
            container.Register<ITypeToResolveWithConstructorParams, ConcreteTypeWithConstructorParams>(LifeCycle.Transient);

            //Act
            var instance = container.Resolve<ITypeToResolveWithConstructorParams>();

            //Assert
            Assert.That(instance, Is.InstanceOf(typeof(ConcreteTypeWithConstructorParams)));
        }

        [Test]
        public void Can_Create_Singleton_Instance()
        {
            //Arrange
            var container = new IoCDemoContainer();
            container.Register<ITypeToResolve, ConcreteType>(LifeCycle.Singleton);

            //Act
            var instance = container.Resolve<ITypeToResolve>();

            //Assert
            Assert.That(container.Resolve<ITypeToResolve>(), Is.SameAs(instance));
        }

        [Test]
        public void Can_Create_Transient_Instance()
        {
            //Arrange
            var container = new IoCDemoContainer();
            container.Register<ITypeToResolve, ConcreteType>(LifeCycle.Transient);

            //Act
            var instance = container.Resolve<ITypeToResolve>();

            //Assert
            Assert.That(container.Resolve<ITypeToResolve>(), Is.Not.SameAs(instance));
        }
    }

    public interface ITypeToResolve
    {
    }

    public class ConcreteType : ITypeToResolve
    {
    }

    public interface ITypeToResolveWithConstructorParams
    {
    }

    public class ConcreteTypeWithConstructorParams : ITypeToResolveWithConstructorParams
    {
        public ConcreteTypeWithConstructorParams(ITypeToResolve typeToResolve)
        {
        }
    }
}