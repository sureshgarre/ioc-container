namespace IoCDemo
{
    public enum LifeCycle
    {
        Singleton,
        Transient
    }
}